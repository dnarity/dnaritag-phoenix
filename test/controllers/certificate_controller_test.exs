defmodule Dnaritag.CertificateControllerTest do
  use Dnaritag.ConnCase

  alias Dnaritag.Certificate
  @valid_attrs %{dnaritag: "some content", status: "some content"}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, certificate_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing certificates"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, certificate_path(conn, :new)
    assert html_response(conn, 200) =~ "New certificate"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, certificate_path(conn, :create), certificate: @valid_attrs
    assert redirected_to(conn) == certificate_path(conn, :index)
    assert Repo.get_by(Certificate, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, certificate_path(conn, :create), certificate: @invalid_attrs
    assert html_response(conn, 200) =~ "New certificate"
  end

  test "shows chosen resource", %{conn: conn} do
    certificate = Repo.insert! %Certificate{}
    conn = get conn, certificate_path(conn, :show, certificate)
    assert html_response(conn, 200) =~ "Show certificate"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, certificate_path(conn, :show, "11111111-1111-1111-1111-111111111111")
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    certificate = Repo.insert! %Certificate{}
    conn = get conn, certificate_path(conn, :edit, certificate)
    assert html_response(conn, 200) =~ "Edit certificate"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    certificate = Repo.insert! %Certificate{}
    conn = put conn, certificate_path(conn, :update, certificate), certificate: @valid_attrs
    assert redirected_to(conn) == certificate_path(conn, :show, certificate)
    assert Repo.get_by(Certificate, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    certificate = Repo.insert! %Certificate{}
    conn = put conn, certificate_path(conn, :update, certificate), certificate: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit certificate"
  end

  test "deletes chosen resource", %{conn: conn} do
    certificate = Repo.insert! %Certificate{}
    conn = delete conn, certificate_path(conn, :delete, certificate)
    assert redirected_to(conn) == certificate_path(conn, :index)
    refute Repo.get(Certificate, certificate.id)
  end
end
