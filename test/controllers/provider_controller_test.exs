defmodule Dnaritag.ProviderControllerTest do
  use Dnaritag.ConnCase

  alias Dnaritag.Provider
  @valid_attrs %{name: "some content"}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, provider_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing providers"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, provider_path(conn, :new)
    assert html_response(conn, 200) =~ "New provider"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, provider_path(conn, :create), provider: @valid_attrs
    assert redirected_to(conn) == provider_path(conn, :index)
    assert Repo.get_by(Provider, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, provider_path(conn, :create), provider: @invalid_attrs
    assert html_response(conn, 200) =~ "New provider"
  end

  test "shows chosen resource", %{conn: conn} do
    provider = Repo.insert! %Provider{}
    conn = get conn, provider_path(conn, :show, provider)
    assert html_response(conn, 200) =~ "Show provider"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, provider_path(conn, :show, "11111111-1111-1111-1111-111111111111")
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    provider = Repo.insert! %Provider{}
    conn = get conn, provider_path(conn, :edit, provider)
    assert html_response(conn, 200) =~ "Edit provider"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    provider = Repo.insert! %Provider{}
    conn = put conn, provider_path(conn, :update, provider), provider: @valid_attrs
    assert redirected_to(conn) == provider_path(conn, :show, provider)
    assert Repo.get_by(Provider, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    provider = Repo.insert! %Provider{}
    conn = put conn, provider_path(conn, :update, provider), provider: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit provider"
  end

  test "deletes chosen resource", %{conn: conn} do
    provider = Repo.insert! %Provider{}
    conn = delete conn, provider_path(conn, :delete, provider)
    assert redirected_to(conn) == provider_path(conn, :index)
    refute Repo.get(Provider, provider.id)
  end
end
