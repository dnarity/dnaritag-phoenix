defmodule Dnaritag.CertificateTest do
  use Dnaritag.ModelCase

  alias Dnaritag.Certificate

  @valid_attrs %{dnaritag: "some content", status: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Certificate.changeset(%Certificate{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Certificate.changeset(%Certificate{}, @invalid_attrs)
    refute changeset.valid?
  end
end
