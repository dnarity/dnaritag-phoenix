defmodule Dnaritag.ValidationTest do
  use Dnaritag.ModelCase

  alias Dnaritag.Validation

  @valid_attrs %{dnaritag: "some content", valid: true}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Validation.changeset(%Validation{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Validation.changeset(%Validation{}, @invalid_attrs)
    refute changeset.valid?
  end
end
