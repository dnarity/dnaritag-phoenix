defmodule Dnaritag.ProviderTest do
  use Dnaritag.ModelCase

  alias Dnaritag.Provider

  @valid_attrs %{name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Provider.changeset(%Provider{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Provider.changeset(%Provider{}, @invalid_attrs)
    refute changeset.valid?
  end
end
