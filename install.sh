#!/bin/bash
set -e

git pull
sudo rm -fr priv/static/*

export MIX_ENV=prod
export PORT=80
mix local.hex --force
yes | head -n 1000 | mix deps.get
yes | head -n 1000 | mix deps.compile
yes | head -n 1000 | mix compile
yes | head -n 1000 | mix ecto.migrate
npm install && node node_modules/brunch/bin/brunch build --production
mix phoenix.digest
