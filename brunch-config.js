exports.config = {
  // See http://brunch.io/#documentation for docs.
  files: {
    javascripts: {
      joinTo: "js/app.js",

      order: {
        before: [
          "dist/jquery.min.js",
          "dist/js/tether.min.js",
          "dist/bloodhound.min.js",
          "dist/typeahead.jquery.min.js"
        ]
      }
    },
    stylesheets: {
      joinTo: "css/app.css",
      order: {
        after: ["web/static/css/app.scss"] // concat app.css last
      }
    },
    templates: {
      joinTo: "js/app.js"
    }
  },

  conventions: {
    assets: /^(web\/static\/assets)/
  },

  // Phoenix paths configuration
  paths: {
    // Dependencies and current project directories to watch
    watched: [
      "web/static",
      "test/static"
    ],

    // Where to compile files to
    public: "priv/static"
  },

  // Configure your plugins
  plugins: {
    babel: {
      // Do not use ES6 compiler in vendor code
      ignore: [
        /web\/static\/vendor/,
        "node_modules/bloodhound-js/dist/bloodhound.min.js",
        "node_modules/typeahead.js/dist/typeahead.jquery.min.js",
        "node_modules/tether/dist/js/tether.min.js",
      ]
    },
    sass: {
      options: {
        includePaths: ["node_modules/bootstrap/scss"],
        precision: 8
      }
    }
  },

  modules: {
    autoRequire: {
      "js/app.js": ["web/static/js/app"]
    }
  },

  npm: {
    enabled: true,
    globals: {
      jQuery: "jquery",
      $: "jquery",
      Tether: "tether",
      bootstrap: 'bootstrap'
    },
    static: [
      "node_modules/bloodhound-js/dist/bloodhound.min.js",
      "node_modules/typeahead.js/dist/typeahead.jquery.min.js",
    ]
  }
};
