defmodule Dnaritag.Router do
  @moduledoc false

  use Dnaritag.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/auth", Dnaritag do
    pipe_through [:browser]

    get "/:provider", AuthController, :request
    get "/:provider/callback", AuthController, :callback
    post "/:provider/callback", AuthController, :callback
    delete "/logout", AuthController, :delete
  end

  scope "/", Dnaritag do
    pipe_through :browser

    get "/privacy", PrivacyController, :index
    get "/term", TermController, :index
    get "/", PageController, :index

    resources "/batches", BatchController, only: [:new, :create]
    resources "/certificates", CertificateController
    resources "/collections", CollectionController
    resources "/items", ItemController
    resources "/providers", ProviderController
    resources "/providers/:provider_id/validations",
      ValidationController, only: [:show, :index]
    resources "/registrations", RegistrationController
    resources "/sessions", SessionController, only: [:new, :create, :delete]
    resources "/validations", ValidationController, only: [:show, :index]
  end

  scope "/api", Dnaritag do
    pipe_through :api
    resources "/providers/:name", ProviderController, only: [:index]
  end
end
