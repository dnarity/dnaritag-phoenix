defmodule Dnaritag.BatchView do
  use Dnaritag.Web, :view

  def items_for_select(items) do
    items
    |> Enum.map(&["#{&1.name}": &1.id])
    |> List.flatten
  end
end
