defmodule Dnaritag.CertificateView do
  use Dnaritag.Web, :view

  def get_certificate_state(certificate) do
    cond do
      certificate.status == "NEW" -> :new
      certificate.status == "OWNED" -> :owned
      certificate.status == "INVALID" -> :invalid
    end
  end

  def markdown(text) do
    text
    |> Earmark.to_html
    |> raw
  end
end
