defmodule Dnaritag.ItemView do
  use Dnaritag.Web, :view

  def providers_for_select(providers) do
    providers
    |> Enum.map(&["#{&1.name}": &1.id])
    |> List.flatten
  end

  def short_description(item) do
    if String.length(item.description) > 40 do
      String.slice(item.description, 0..40) <> "..."
    else
      item.description
    end
  end
end
