defmodule Dnaritag.TermController do
  @moduledoc """
  Terms and conditions page
  """
  use Dnaritag.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
