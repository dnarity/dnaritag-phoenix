defmodule Dnaritag.ValidationController do
  @moduledoc """
  Validation Controller
  """

  use Dnaritag.Web, :controller
  import Dnaritag.Certification
  import Dnaritag.Login

  def index(conn, %{"provider_id" => provider_id}) do
    provider = get_provider!(provider_id)
    render conn, "index.html", provider: provider
  end

  def show(conn, %{"provider_id" => provider_id, "id" => dnaritag}) do
    certificate = get_certificate(conn, provider_id, dnaritag)
    conn = log_validation(conn, %{certificate: certificate,
                           provider_id: provider_id, dnaritag: dnaritag})
    check_user(conn)
    if certificate do
      redirect conn, to: certificate_path(conn, :show, certificate)
    else
      provider = get_provider!(provider_id)
      conn
      |> put_flash(:error, "No pude validar el certificado")
      |> render("index.html", provider: provider)
    end
  end
end
