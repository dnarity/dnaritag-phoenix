defmodule Dnaritag.PageController do
  @moduledoc """
  Page Controller
  """

  use Dnaritag.Web, :controller
  import Dnaritag.Certification

  def index(conn, _params) do
    validation = get_current_validation(conn)
    if validation do
      action_or_provider(conn, create_validation_path(conn, validation))
    else
      action_or_provider(conn, collection_path(conn, :index))
    end
  end

  defp create_validation_path(conn, validation) do
    validation_path(conn, :show, validation.provider_id, validation.dnaritag)
  end

  defp action_or_provider(conn, action) do
    if get_session(conn, :current_user) do
      redirect conn, to: action
    else
      redirect conn, to: provider_path(conn, :index)
    end
  end
end
