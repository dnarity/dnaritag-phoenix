defmodule Dnaritag.ProviderController do
  @moduledoc """
  Provider Controller
  """

  use Dnaritag.Web, :controller

  alias Dnaritag.Provider
  import Ecto.Query

  def index(conn, %{"name" => name}) do
    query = from p in Provider,
      where: like(p.name, ^"%#{name}%"),
      limit: 8
    providers = Repo.all(query)
    json conn, providers
  end
  def index(conn, _params) do
    render(conn, "index.html")
  end

  def new(conn, _params) do
    changeset = Provider.changeset(%Provider{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"provider" => provider_params}) do
    changeset = Provider.changeset(%Provider{}, provider_params)

    case Repo.insert(changeset) do
      {:ok, _provider} ->
        conn
        |> put_flash(:info, "Provider created successfully.")
        |> redirect(to: provider_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    provider = Repo.get!(Provider, id)
    render(conn, "show.html", provider: provider)
  end

  def edit(conn, %{"id" => id}) do
    provider = Repo.get!(Provider, id)
    changeset = Provider.changeset(provider)
    render(conn, "edit.html", provider: provider, changeset: changeset)
  end

  def update(conn, %{"id" => id, "provider" => provider_params}) do
    provider = Repo.get!(Provider, id)
    changeset = Provider.changeset(provider, provider_params)

    case Repo.update(changeset) do
      {:ok, provider} ->
        conn
        |> put_flash(:info, "Provider updated successfully.")
        |> redirect(to: provider_path(conn, :show, provider))
      {:error, changeset} ->
        render(conn, "edit.html", provider: provider, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    provider = Repo.get!(Provider, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(provider)

    conn
    |> put_flash(:info, "Provider deleted successfully.")
    |> redirect(to: provider_path(conn, :index))
  end
end
