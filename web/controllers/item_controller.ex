defmodule Dnaritag.ItemController do
  use Dnaritag.Web, :controller

  alias Dnaritag.Item
  alias Dnaritag.Provider

  def index(conn, _params) do
    items = Repo.all(Item)
    render(conn, "index.html", items: items)
  end

  def new(conn, _params) do
    providers = Repo.all(Provider)
    changeset = Item.changeset(%Item{})
    render(conn, "new.html", changeset: changeset, providers: providers)
  end

  def create(conn, %{"item" => item_params}) do
    providers = Repo.all(Provider)
    changeset = Item.changeset(%Item{}, item_params)

    case Repo.insert(changeset) do
      {:ok, _item} ->
        conn
        |> put_flash(:info, "Item created successfully.")
        |> redirect(to: item_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset, providers: providers)
    end
  end

  def show(conn, %{"id" => id}) do
    item = Repo.get!(Item, id) |> Repo.preload(:provider)
    render(conn, "show.html", item: item)
  end

  def edit(conn, %{"id" => id}) do
    providers = Repo.all(Provider)
    item = Repo.get!(Item, id)
    changeset = Item.changeset(item)
    render(conn, "edit.html", item: item, changeset: changeset, providers: providers)
  end

  def update(conn, %{"id" => id, "item" => item_params}) do
    providers = Repo.all(Provider)
    item = Repo.get!(Item, id)
    changeset = Item.changeset(item, item_params)

    case Repo.update(changeset) do
      {:ok, item} ->
        conn
        |> put_flash(:info, "Item updated successfully.")
        |> redirect(to: item_path(conn, :show, item))
      {:error, changeset} ->
        render(conn, "edit.html", item: item, changeset: changeset, providers: providers)
    end
  end

  def delete(conn, %{"id" => id}) do
    item = Repo.get!(Item, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(item)

    conn
    |> put_flash(:info, "Item deleted successfully.")
    |> redirect(to: item_path(conn, :index))
  end
end
