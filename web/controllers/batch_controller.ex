defmodule Dnaritag.BatchController do
  use Dnaritag.Web, :controller
  require Logger

  @max_length 9

  alias Dnaritag.Batch
  alias Dnaritag.Item
  alias Dnaritag.Certificate

  def new(conn, _params) do
    items = Repo.all(Item)
    changeset = Batch.changeset(%Batch{})
    render(conn, "new.html", changeset: changeset, items: items)
  end

  def create(conn, %{"batch" => batch_params}) do
    items = Repo.all(Item)
    changeset = Batch.changeset(%Batch{}, batch_params)

    case create_batch(changeset) do
      {:ok, _batch} ->
        conn
        |> put_flash(:info, "Batch created successfully.")
        |> redirect(to: certificate_path(conn, :create))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset, items: items)
    end
  end

  defp create_batch(changeset) do
    case Repo.insert(changeset) do
      {:ok, batch} ->
        create_certificates(changeset, batch)
      {:error, changeset} ->
        {:error, changeset}
    end
  end

  defp create_certificates(changeset, batch) do
    certs = String.split(changeset.changes.tags, "\n")
    |> Enum.filter_map(&certificate_filter/1, &(create_certificate(&1, changeset, batch)))
    Repo.insert_all(Certificate, certs)
    {:ok, batch}
  end

  defp certificate_filter(dnaritag), do: String.length(dnaritag) >= @max_length

  defp create_certificate(dnaritag, changeset, batch) do
    [status: "NEW",
     dnaritag: String.slice(dnaritag, 0..(@max_length - 1)),
     batch_id: batch.id,
     item_id: changeset.changes.item_id,
     inserted_at: Ecto.DateTime.utc,
     updated_at: Ecto.DateTime.utc
    ]
  end
end
