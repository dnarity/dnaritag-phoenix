defmodule Dnaritag.RegistrationController do
  @moduledoc """
  Registration Controller
  """

  use Dnaritag.Web, :controller
  alias Dnaritag.Certificate
  import Dnaritag.Login
  require Logger

  def index(conn, _params) do
    certificate = get_session(conn, :certificate)
    user = check_user(conn)
    validate_certificate(conn, certificate, user)
  end

  def show(conn, %{"id" => id}) do
    certificate = Repo.get_by!(Certificate, id: id, status: "NEW")
    user = check_user(conn)
    register_certificate(conn, certificate, user)
  end

  defp validate_certificate(conn, certificate, user) do
    Logger.info "Validating Certificate"
    if certificate do
      register_certificate(conn, certificate, user)
    else
      redirect conn, to: collection_path(conn, :index)
    end
  end

  defp register_certificate(conn, certificate, user) do
    Logger.info(
      "Registering certificate: #{certificate.id} with user: #{user.id}"
    )
    certificate
      |> Ecto.Changeset.change([user_id: user.id, status: "OWNED"])
      |> Repo.update!
    delete_session(conn, :certificate)
    redirect(conn, to: collection_path(conn, :index))
  end
end
