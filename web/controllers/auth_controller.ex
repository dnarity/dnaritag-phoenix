defmodule Dnaritag.AuthController do
  @moduledoc """
  Auth controller responsible for handling Ueberauth responses
  """

  use Dnaritag.Web, :controller
  plug Ueberauth

  alias Ueberauth.Strategy.Helpers
  alias Dnaritag.UserFromAuth

  def request(conn, _params) do
    render(conn, "request.html", callback_url: Helpers.callback_url(conn))
  end

  def delete(conn, _params) do
    conn
    |> put_flash(:info, "Salió del sistema.")
    |> configure_session(drop: true)
    |> redirect(to: page_path(conn, :index))
  end

  def callback(%{assigns: %{ueberauth_failure: _fails}} = conn, _params) do
    conn
    |> put_flash(:error, "No pudimos hacer login con tu cuenta.")
    |> redirect(to: page_path(conn, :index))
  end

  def callback(%{assigns: %{ueberauth_auth: auth}} = conn, _params) do
    case UserFromAuth.find_or_create(auth) do
      {:ok, user} ->
        conn
        |> put_session(:current_user, user)
        |> redirect(to: page_path(conn, :index))
      {:error, reason} ->
        conn
        |> put_flash(:error, reason)
        |> redirect(to: page_path(conn, :index))
    end
  end
end
