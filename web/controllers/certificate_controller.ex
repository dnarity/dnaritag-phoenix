defmodule Dnaritag.CertificateController do
  @moduledoc """
  Certificate Controller
  """

  use Dnaritag.Web, :controller
  import Dnaritag.Login

  alias Dnaritag.Certificate

  def index(conn, _params) do
    certificates = Repo.all(Certificate)
    render(conn, "index.html", certificates: certificates)
  end

  def new(conn, _params) do
    changeset = Certificate.changeset(%Certificate{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"certificate" => certificate_params}) do
    changeset = Certificate.changeset(%Certificate{}, certificate_params)

    case Repo.insert(changeset) do
      {:ok, _certificate} ->
        conn
        |> put_flash(:info, "Certificate created successfully.")
        |> redirect(to: certificate_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    check_user(conn)
    certificate = Certificate |> Repo.get!(id) |> Repo.preload(item: :provider)
    render(conn, "show.html", certificate: certificate,
      item: certificate.item, provider: certificate.item.provider)
  end

  def edit(conn, %{"id" => id}) do
    certificate = Repo.get!(Certificate, id)
    changeset = Certificate.changeset(certificate)
    render(conn, "edit.html", certificate: certificate, changeset: changeset)
  end

  def update(conn, %{"id" => id, "certificate" => certificate_params}) do
    certificate = Repo.get!(Certificate, id)
    changeset = Certificate.changeset(certificate, certificate_params)

    case Repo.update(changeset) do
      {:ok, certificate} ->
        conn
        |> put_flash(:info, "Certificate updated successfully.")
        |> redirect(to: certificate_path(conn, :show, certificate))
      {:error, changeset} ->
        render(conn, "edit.html", certificate:
          certificate, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    certificate = Repo.get!(Certificate, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(certificate)

    conn
    |> put_flash(:info, "Certificate deleted successfully.")
    |> redirect(to: certificate_path(conn, :index))
  end
end
