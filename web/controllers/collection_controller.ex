defmodule Dnaritag.CollectionController do
  @moduledoc """
  Collection Controller
  """

  use Dnaritag.Web, :controller
  import Dnaritag.Certification
  import Dnaritag.Login

  def index(conn, _params) do
    user = check_user(conn)
    certificates = get_certificates(user.id)
    render conn, "index.html", certificates: certificates
  end
end
