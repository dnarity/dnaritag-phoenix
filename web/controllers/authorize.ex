defmodule Dnaritag.Authorize do

  import Plug.Conn
  import Phoenix.Controller
  import Dnaritag.Router.Helpers

  @not_logged "Necesitas hacer login para ver esta página"
  @not_authorized "No tienes permiso para ver esta página"

  def auth_action(%Plug.Conn{assigns: %{current_user: nil}} = conn, _) do
    auth_error conn, @not_logged, session_path(conn, :new)
  end
  def auth_action(%Plug.Conn{assigns: %{current_user: current_user},
    params: params} = conn, module) do
    apply(module, action_name(conn), [conn, params, current_user])
  end

  def user_check(%Plug.Conn{assigns: %{current_user: nil}} = conn, _opts) do
    auth_error conn, @not_logged, session_path(conn, :new)
  end
  def user_check(conn, _opts), do: conn

  def id_check(%Plug.Conn{assigns: %{current_user: nil}} = conn, _opts) do
    auth_error conn, @not_logged, session_path(conn, :new)
  end
  # def id_check(%Plug.Conn{params: %{"id" => id}, assigns: %{current_user:
  #    %{id: current_id}}} = conn, _opts) do
  #   if id == to_string(current_id), do: conn,
  #    else: auth_error conn, @not_authorized, user_path(conn, :index)
  # end

  def auth_info(conn, message, path) do
    conn
    |> put_flash(:info, message)
    |> redirect(to: path)
  end

  def auth_error(conn, message, path) do
    conn
    |> put_flash(:error, message)
    |> redirect(to: path)
    |> halt
  end
end
