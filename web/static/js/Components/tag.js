import $ from 'jquery';
const CERT_SIZE = 9;

const cert = [null, null, null, null, null, null, null, null, null];

const wrapInDiv = (rows) => {
  const grid = $("<div>");
  rows.forEach((r) => grid.append(r));
  return grid;
};

const createRows = (n) => n.map(() => $("<div>", { class: "row justify-content-center" }));

const buildBar = (brand) => {
  const buildClass = (n) => ({ class: `dna-clickable square square-${n}` });
  const rows = createRows(["", ""]);
  for (let i = 0; i <= 9; i++) {
    const svg = `<svg><use href="/images/providers/${brand}/icons/${i}.svg#Layer_1"></use></svg>`;
    rows[i % 2].append($("<div>", buildClass(i)));
  }
  return wrapInDiv(rows);
};

const buildGrid = () => {
  const rows = createRows(["", "", ""]);
  const attr = { class: "square dna-fillable" };
  for(var i = 0; i < 9; i++) {
    attr.id = `pos-${i}`;
    if (i < 3) {
      rows[0].append($("<div>", attr));
    }
    else if (i < 6) {
      rows[1].append($("<div>", attr));
    }
    else {
      rows[2].append($("<div>", attr));
    }
  }
  return wrapInDiv(rows);
};

const setIcon = (brand, dnaClickable, dnaFillable) => {
  console.log('dnaClickable:=', dnaClickable);
  if(dnaClickable === undefined) return;
  const ICON_POS = 2;
  const icon = dnaClickable.className.split(" ")[ICON_POS];
  const icon_name = icon.split("-")[1];
  const position = dnaFillable.id.split("-")[1];
  dnaFillable.className = `square dna-fillable border-0 square-${icon_name}`;
  cert[position] = icon_name;
  const fullCert = cert.join("");
  if(fullCert.length == CERT_SIZE) {
    const validateButton = $("a#dna-validate-button");
    validateButton.attr("href", `/providers/${brand}/validations/${cert.join("")}`);
  }
};

const initSelector = (brand) => {
  $(".dna-clickable").click((e) => {
    $(".dna-clickable").removeClass("dna-selected-square");
    $(e.target).addClass("dna-selected-square");
  });
  $(".dna-fillable").click((e) => {
    setIcon(brand, $(".dna-selected-square")[0], $(e.target)[0]);
  });
};

const Tag = {
  buildBar,
  initSelector,
  buildGrid
};

export default Tag;

// const CERT_SIZE = 9;

// const cert = [null, null, null, null, null, null, null, null, null];

