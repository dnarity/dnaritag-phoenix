import $ from 'jquery';

const mount = () => {
  console.log("Mounting this thing! - Show Item");
  const id = $('#provider_id').val();
  console.log("id = ", id);

  $('#dna').css("background", `url('/images/providers/${id}.jpg')`);
};

const ShowPage = {
  mount,
};

export default ShowPage;
