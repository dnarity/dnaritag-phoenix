const templates = {
  empty: [
    `<div class="list-group search-results-dropdown">
      <div class="list-group-item">No tenemos registrada esa marca</div>
    </div>`
  ]
};

const charMap = {
  "àâá": "a", "éèêë": "e", "ïîí": "i", "ôöó": "o", "ûùú": "u", "ñ": "n"
};

const normalize = str => {
  let result = str;
  Object.keys(charMap).forEach(chars => {
    const regex = new RegExp(`[${chars}]`, 'gi');
    result = result.replace(regex, charMap[chars]);
  });
  return result;
};

const queryTokenizer = q => Bloodhound.tokenizers.whitespace(normalize(q));
const datumTokenizer = obj => {
  obj.normalized = normalize(obj.name);
  return Bloodhound.tokenizers.obj.whitespace('normalized')(obj);
};
const prepare = (q, { url, type, dataType }) => {
  return { url: `${url}/${normalize(q)}`, type, dataType };
};

const engine = new Bloodhound({
  remote: {
    url: '/api/providers',
    prepare
  },
  queryTokenizer,
  datumTokenizer
});

const dataset = {
  name: 'providers',
  source: engine.ttAdapter(),
  limit: 1000,
  display: 'name',
  templates
};

const options = {
  hint: true,
  highlight: true,
  minLength: 3
};

const init = (selector, fn) => {
  const searchBox = $(selector);
  searchBox.typeahead(options, dataset)
    .bind('typeahead:select', fn)
    .focus();
};

export default init;

