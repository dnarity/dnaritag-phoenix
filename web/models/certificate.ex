defmodule Dnaritag.Certificate do
  use Dnaritag.Web, :model

  schema "certificates" do
    field :status, :string
    field :dnaritag, :string
    belongs_to :batch, Dnaritag.Batch
    belongs_to :item, Dnaritag.Item
    belongs_to :user, Dnaritag.User

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:status, :dnaritag, :batch_id, :item_id])
    |> validate_required([:status, :dnaritag, :batch_id, :item_id])
  end
end
