defmodule Dnaritag.UserFromAuth do
  @moduledoc """
  Retrieve the user information from an auth request
  """

  alias Ueberauth.Auth
  alias Dnaritag.User
  alias Dnaritag.Repo

  def find_or_create(%Auth{} = auth) do
    {:ok, auth |> basic_info |> save_db_user}
  end

  defp save_db_user(user) do
    db_user =
      case Repo.get_by(User, provider_uid: user.provider_uid) do
        nil -> %User{}
        u -> u
      end
    user
      |> Map.put(:is_new, is_nil(db_user.id))
      |> user_insert_or_update(db_user)
  end

  defp user_insert_or_update(user, db_user) do
    changed_user = db_user
      |> User.changeset(%{provider_uid: user.provider_uid, email: user.email,
                        provider: user.provider})
      |> Repo.insert_or_update!
    user
      |> Map.put(:id, changed_user.id)
  end

  defp basic_info(auth) do
    %{provider_uid: auth.uid, name: name_from_auth(auth),
      avatar: auth.info.image, email: auth.info.email,
      provider: Atom.to_string(auth.provider)}
  end

  defp name_from_auth(auth) do
    if auth.info.name do
      auth.info.name
    else
      name = [auth.info.first_name, auth.info.last_name]
      |> Enum.filter(&(&1 != nil and &1 != ""))

      cond do
        length(name) == 0 -> auth.info.nickname
        true -> Enum.join(name, " ")
      end
    end
  end
end
