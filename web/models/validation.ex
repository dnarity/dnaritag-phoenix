defmodule Dnaritag.Validation do
  use Dnaritag.Web, :model

  schema "validations" do
    field :dnaritag, :string
    field :ip, :string
    field :valid, :boolean, default: false

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:dnaritag, :valid, :ip])
    |> validate_required([:dnaritag, :valid, :ip])
  end
end
