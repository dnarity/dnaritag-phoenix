defmodule Dnaritag.Item do
  use Dnaritag.Web, :model

  schema "items" do
    field :name, :string
    field :description, :string
    belongs_to :provider, Dnaritag.Provider

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :description, :provider_id])
    |> validate_required([:name, :description, :provider_id])
  end
end
