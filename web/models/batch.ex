defmodule Dnaritag.Batch do
  use Dnaritag.Web, :model

  schema "batches" do
    field :tags, :string, virtual: true
    field :item_id, :string, virtual: true

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:tags, :item_id])
    |> validate_required([:tags, :item_id])
  end
end
