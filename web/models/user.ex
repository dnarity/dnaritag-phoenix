defmodule Dnaritag.User do
  @moduledoc """
  User
  """

  use Dnaritag.Web, :model

  schema "users" do
    field :email, :string
    field :provider, :string
    field :provider_uid, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:email, :provider, :provider_uid])
    |> validate_required([:provider, :provider_uid])
  end
end
