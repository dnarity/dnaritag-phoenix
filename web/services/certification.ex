defmodule Dnaritag.Certification do
  @moduledoc """
  Certification tasks:
  Provider
  """

  alias Dnaritag.{Repo, Provider, Validation, Certificate}
  import Plug.Conn, only: [get_session: 2, put_session: 3, delete_session: 2]
  import Ecto.Query
  require IEx

  def get_provider!(id), do: Repo.get!(Provider, id)

  def get_certificate(conn, provider_id, dnaritag) do
    validation = get_current_validation(conn)
    if validation do
      get_certificate_from_id(validation.certificate_id)
    else
      get_certificate_from_dnaritag(provider_id, dnaritag)
    end
  end

  defp get_certificate_from_id(:nil), do: :nil
  defp get_certificate_from_id(id), do: Repo.get(Certificate, id)

  defp get_certificate_from_dnaritag(provider_id, dnaritag) do
    q = from c in Certificate,
      join: i in assoc(c, :item),
      join: p in assoc(i, :provider),
      where: p.id == ^provider_id and c.dnaritag == ^dnaritag
    Repo.one(q)
  end

  def get_certificates(user_id) do
    q = from c in Certificate,
      where: c.user_id == ^user_id,
      preload: [item: :provider]
    Repo.all(q)
  end

  def log_validation(conn, current_validation) do
    if get_current_validation(conn) do
      delete_session(conn, :current_validation)
    else
      log_validation_to_db(conn, current_validation)
      put_session(conn, :current_validation,
        %{certificate_id: get_certificate_id(current_validation.certificate),
          provider_id: current_validation.provider_id,
          dnaritag: current_validation.dnaritag})
    end
  end

  def get_certificate_id(:nil), do: :nil
  def get_certificate_id(certificate), do: certificate.id

  def log_validation_to_db(conn, current_validation) do
    certificate_is_valid = current_validation.certificate != :nil
    validation = Validation.changeset(
      %Validation{}, %{:dnaritag => current_validation.dnaritag,
                       :valid => certificate_is_valid,
                       :ip => get_parsed_ip(conn)})
    Repo.insert!(validation)
  end

  def get_current_validation(conn) do
    get_session(conn, :current_validation)
  end

  defp get_parsed_ip(conn) do
    conn.remote_ip
    |> Tuple.to_list
    |> Enum.join(".")
  end
end
