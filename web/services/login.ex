defmodule Dnaritag.Login do
  @moduledoc """
  Helper functions to User models
  """

  import Plug.Conn, only: [get_session: 2, halt: 1]
  import Phoenix.Controller, only: [put_flash: 3, redirect: 2]
  import Dnaritag.Router.Helpers

  def check_user(conn) do
    user = get_session(conn, :current_user)
    if user do
      IO.inspect user
      user
    else
      conn
      |> put_flash(:error, "Debe estar inscrito en www.dnaritag.com")
      |> redirect(to: session_path(conn, :new))
      |> halt
    end
  end
end
