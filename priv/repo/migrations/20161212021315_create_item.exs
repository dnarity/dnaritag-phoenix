defmodule Dnaritag.Repo.Migrations.CreateItem do
  use Ecto.Migration

  def change do
    create table(:items, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :description, :text
      add :provider_id, references(:providers, on_delete: :nothing, type: :binary_id)

      timestamps()
    end
    create index(:items, [:provider_id])

  end
end
