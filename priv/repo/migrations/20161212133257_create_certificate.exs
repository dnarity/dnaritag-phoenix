defmodule Dnaritag.Repo.Migrations.CreateCertificate do
  use Ecto.Migration

  def change do
    create table(:certificates, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :status, :string
      add :dnaritag, :string
      add :batch_id, references(:batches, on_delete: :nothing, type: :binary_id)
      add :item_id, references(:items, on_delete: :nothing, type: :binary_id)
      add :user_id, references(:users, on_delete: :nothing, type: :binary_id)

      timestamps()
    end
    create index(:certificates, [:batch_id])
    create index(:certificates, [:item_id])
    create index(:certificates, [:user_id])

  end
end
