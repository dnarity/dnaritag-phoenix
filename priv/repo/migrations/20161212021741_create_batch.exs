defmodule Dnaritag.Repo.Migrations.CreateBatch do
  use Ecto.Migration

  def change do
    create table(:batches, primary_key: false) do
      add :id, :binary_id, primary_key: true

      timestamps()
    end

  end
end
