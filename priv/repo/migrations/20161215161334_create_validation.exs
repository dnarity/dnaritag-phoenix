defmodule Dnaritag.Repo.Migrations.CreateValidation do
  use Ecto.Migration

  def change do
    create table(:validations, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :dnaritag, :string
      add :valid, :boolean, default: false, null: false

      timestamps()
    end

  end
end
