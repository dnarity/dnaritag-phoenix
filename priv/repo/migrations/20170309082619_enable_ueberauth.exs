defmodule Dnaritag.Repo.Migrations.EnableUeberauth do
  use Ecto.Migration

  def change do
    drop index(:users, [:username])
    alter table(:users) do
      add :provider, :string
      add :provider_uid, :string
      remove :password_hash
      remove :username
    end
    create unique_index(:users, [:provider_uid])
  end
end
