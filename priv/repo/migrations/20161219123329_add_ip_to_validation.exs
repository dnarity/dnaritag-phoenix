defmodule Dnaritag.Repo.Migrations.AddIpToValidation do
  use Ecto.Migration

  def change do
    alter table(:validations) do
      add :ip, :string
    end
  end
end
