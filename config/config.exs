# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :dnaritag,
  ecto_repos: [Dnaritag.Repo]

# Configures the endpoint
config :dnaritag, Dnaritag.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "5QOkdK90eVHs0Q1DPMqukceGc9GuiuaRTWlWlyh++ipoKNaUURiGAxg8RkrbUBj7",
  render_errors: [view: Dnaritag.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Dnaritag.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configure phoenix generators
config :phoenix, :generators,
  binary_id: true

config :ueberauth, Ueberauth,
  providers: [
    google: { Ueberauth.Strategy.Google, [] },
    facebook: { Ueberauth.Strategy.Facebook,
                [default_scope: "email,public_profile",
                 profile_fields: "name,email"]}
  ]

config :dogma,
  rule_set: Dogma.RuleSet.All

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
