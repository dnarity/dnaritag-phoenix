use Mix.Config

# For development, we disable any cache and enable
# debugging and code reloading.
#
# The watchers configuration can be used to run external
# watchers to your application. For example, we use it
# with brunch.io to recompile .js and .css sources.
config :dnaritag, Dnaritag.Endpoint,
  http: [port: 4000],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: [node: ["node_modules/brunch/bin/brunch", "watch", "--stdin",
                    cd: Path.expand("../", __DIR__)]]


# Watch static and templates for browser reloading.
config :dnaritag, Dnaritag.Endpoint,
  live_reload: [
    patterns: [
      ~r{priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$},
      ~r{priv/gettext/.*(po)$},
      ~r{web/views/.*(ex)$},
      ~r{web/templates/.*(eex)$}
    ]
  ]

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

# Configure your database
config :dnaritag, Dnaritag.Repo,
  adapter: Ecto.Adapters.MySQL,
  username: "root",
  password: "HunCame207+",
  port: 5555,
  database: "dnaritag_dev",
  hostname: "localhost",
  pool_size: 10

config :ueberauth, Ueberauth.Strategy.Google.OAuth,
  # client_id: System.get_env("GOOGLE_CLIENT_ID"),
  client_id:
"121020093634-9l1ismpvuv083163e0j6jg1c4d081e8p.apps.googleusercontent.com",
  # client_secret: System.get_env("GOOGLE_CLIENT_SECRET"),
  client_secret: "vlbpbknLBWDhV0K07kOGsTu5",
  # redirect_uri: System.get_env("http://dnarity360.hopto.me:4000/")
  redirect_uri: "http://tipninja.info"

config :ueberauth, Ueberauth.Strategy.Facebook.OAuth,
  # client_id: System.get_env("FACEBOOK_APP_ID"),
  client_id: "1772823972959455",
  # client_secret: System.get_env("FACEBOOK_APP_SECRET")
  client_secret: "8af47459b4dcc870bfd259a2fc37d9e9"

config :logger, format: "[$level] $message\n",
  backends: [{LoggerFileBackend, :error_log}, :console]

config :logger, :error_log,
  path: "log/error.log",
  level: :error
