#!/bin/bash

set -e -x
wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb
dpkg -i erlang-solutions_1.0_all.deb
curl -sL https://deb.nodesource.com/setup_6.x -o nodesource_setup.sh
bash nodesource_setup.sh
apt update
apt upgrade -y
apt install esl-erlang elixir python-pip git mysql-client ruby nodejs wget -y
wget https://aws-codedeploy-us-east-1.s3.amazonaws.com/latest/install
chmod +x install
./install auto
pip install awscli
dd if=/dev/zero of=/var/swapfile bs=2M count=512
chmod 600 /var/swapfile
mkswap /var/swapfile
echo /var/swapfile none swap defaults 0 0 | tee -a /etc/fstab
swapon -a
